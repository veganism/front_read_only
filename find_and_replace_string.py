


import ships.paths.directory.find_and_replace_string_v2 as find_and_replace_string_v2


import pathlib
from os.path import dirname, join, normpath
this_folder = pathlib.Path (__file__).parent.resolve ()

the_path = str (this_folder) + "/vite_module/src"

print ("the_path:", the_path)

proceeds = find_and_replace_string_v2.start ( 
	the_path = the_path,

	find = 'FIELD',
	replace_with = 'field',
	
	replace_contents = "yes",
	replace_paths = "yes"
)

import rich
rich.print_json (data = proceeds)