

'''
	python3 start.py "run vitest"
'''


def parse_sys_args ():
	import sys
	print(sys.argv)

	script_parts = []
	s = 1
	last_index = len (sys.argv) - 1;
	while s <= last_index:
		script_parts.append (sys.argv [s])
		
		s += 1

	script = " ".join (script_parts)

	#print ("script:", script)
	
	return script;





def build_CWD ():
	import pathlib
	from os.path import dirname, join, normpath
	import sys

	this_directory = pathlib.Path (__file__).parent.resolve ()
	CWD = normpath (join (this_directory, "vite_module"))

	return CWD

def build_environment ():
	import pathlib
	from os.path import dirname, join, normpath
	this_directory = pathlib.Path (__file__).parent.resolve ()

	import os
	environment = os.environ.copy ()
	environment ["PATH"] = environment ["PATH"] + ":" + ":".join ([
		str (normpath (join (this_directory, "modules_fedora/node-v18.17.0-linux-x64/bin"))),
		str (normpath (join (this_directory, "modules_fedora/yarn-v1.22.19/bin")))
	])
	
	return environment

import subprocess
subprocess.run (
	"yarn " + parse_sys_args (), 
	
	cwd = build_CWD (),
	env = build_environment (),
	
	shell = True, 
	check = True
)


