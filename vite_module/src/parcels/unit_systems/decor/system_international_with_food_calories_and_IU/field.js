

/*

*/

import { mapState } from 'pinia'

import UNIT_SYSTEM_SELECT from '@/decor/UNIT_SYSTEM_SELECT/field.vue'
import OUTER_LINK from '@/decor/LINK/OUTER.vue'

export const field = {
	components: {
		UNIT_SYSTEM_SELECT,
		OUTER_LINK
	}
}