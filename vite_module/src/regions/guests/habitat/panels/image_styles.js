

export const image_styles = {
	position: 'absolute',
	top: '0',
	left: '50%',
	transform: 'translateX(-50%) scaleX(-1)',
	
	height: '100%',
	width: '100%',
	maxWidth: '1600px',
	
	backgroundRepeat: 'no-repeat',
	backgroundSize: 'cover',
	backgroundColor: 'white',						
}