


import { fileURLToPath, URL } 	from 'node:url'

import { defineConfig } 		from 'vite'
import vue 						from '@vitejs/plugin-vue'
import nightwatchPlugin 		from 'vite-plugin-nightwatch'

const version__ = "12"
const build = {
	rollupOptions: {
		output: {
			entryFileNames: `[name].js`,
			
			chunkFileNames: `${ version__ }_[name].js`,
			assetFileNames: `${ version__ }_[name].[ext]`
		}
	}
}

/*
	https://rollupjs.org/configuration-options/#output-assetfilenames
*/
// https://vitejs.dev/config/
export default defineConfig ({
	build,
	
	plugins: [
		vue (),
		nightwatchPlugin (),
	],
	resolve: {
		alias: {
			'@': fileURLToPath (
				new URL (
					'./src', 
					import.meta.url
				)
			)
		}
	}
})
